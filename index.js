
let result = document.getElementById('result')
let operation = document.getElementById('operation')
let buttons = Array.from(document.getElementsByClassName('button'))

buttons.map( button => {
	button.addEventListener('click', (e) => {
		switch(e.target.innerText) {
			case 'C':
				operation.innerText = '';
				result.innerText = '';
				break;
			case '=':
				try {
					result.innerText += eval(operation.innerText);
				} catch {
					operation.innerText = 'Error'
				}
				break;
			default:
				operation.innerText += e.target.innerText;

		}
	})
})